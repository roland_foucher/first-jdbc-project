package co.simplon.promo16.entity;

import java.time.LocalDate;


public class Dog {
    private Integer id;
    private String breed;
    private String name;
    private LocalDate birstday;

    // constructors

    public Dog(String breed, String name, LocalDate birstday) {
        this.breed = breed;
        this.name = name;
        this.birstday = birstday;
    }
    public Dog(Integer id, String breed, String name, LocalDate birstday) {
        this.id = id;
        this.breed = breed;
        this.name = name;
        this.birstday = birstday;
    }
    public Dog() {
    }

    // getter and setter

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public LocalDate getBirstday() {
        return birstday;
    }
    public void setBirstday(LocalDate birstday) {
        this.birstday = birstday;
    }
    @Override
    public String toString() {
        return "Dog [birstday=" + birstday + ", breed=" + breed + ", id=" + id + ", name=" + name + "]";
    }

}
