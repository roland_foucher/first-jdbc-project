package co.simplon.promo16.repository;

import java.sql.ResultSet;
import java.util.List;

import co.simplon.promo16.entity.Dog;

public interface IDogRepository {
    List <Dog> findAll();
    Dog find(Integer id);
    boolean save(Dog dog);
    boolean update(Dog dog);
    boolean delete(Integer Id);
    
}
