package co.simplon.promo16;

import co.simplon.promo16.entity.Dog;
import co.simplon.promo16.repository.DogRepository;
import co.simplon.promo16.repository.IDogRepository;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        IDogRepository repo = new DogRepository();
        System.out.println(repo.find(5));
        
        Dog dog = repo.find(9);
        dog.setName("snoopDoggyDog");

        repo.update(dog);

        
    }
}
